from string import digits
from django.db import models
from django.contrib.auth.models import User

class CarCategory(models.Model):
    title = models.CharField(max_length=32)

    def __str__(self):
        return self.title

class Car(models.Model):
    title = models.CharField(max_length=32)
    description = models.TextField(blank=True, null=True)
    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=20, decimal_places=0)
    image = models.ImageField(upload_to='car-images')
    related_user = models.ForeignKey(User, on_delete=models.CASCADE)
    related_car_category = models.ForeignKey(CarCategory, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.title

class Comment(models.Model):
    content = models.TextField()
    related_car = models.ForeignKey(Car, on_delete=models.CASCADE)
    related_user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.content