from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from cars.forms import CarForm
from .models import Car, Comment


@login_required
def index(request):
    if request.method == "POST":
        form = CarForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            form.save()
    else:
        form = CarForm()
    cars = Car.objects.filter()
    title = "Anonymous User"
    if request.user.is_authenticated:
        title = f'{request.user.first_name} {request.user.last_name}'
    context = {
        'title': title,
        'cars': cars,
        'form': form
    }
    return render(request, 'index.html', context)

def comments(request):
    instances = Comment.objects.filter(related_car__related_car_category__title='Super Sport')
    context = {
        'comments': instances
    }
    return render(request, 'comments.html', context)